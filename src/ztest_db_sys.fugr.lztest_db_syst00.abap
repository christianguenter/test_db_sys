*---------------------------------------------------------------------*
*    view related data declarations
*---------------------------------------------------------------------*
*...processing: ZTEST_DB_SYS....................................*
DATA:  BEGIN OF STATUS_ZTEST_DB_SYS                  .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZTEST_DB_SYS                  .
CONTROLS: TCTRL_ZTEST_DB_SYS
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZTEST_DB_SYS                  .
TABLES: ZTEST_DB_SYS                   .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
